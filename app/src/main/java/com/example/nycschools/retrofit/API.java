package com.example.nycschools.retrofit;

import com.example.nycschools.model.SchoolsDetailsResponse;
import com.example.nycschools.model.SchoolsListResponse;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface API {
    @GET("s3k6-pzi2.json")
    Single<List<SchoolsListResponse>> getSchools();

    @GET("f9bf-2cp4.json")
    Single<List<SchoolsDetailsResponse>> getDetails(@Query("dbn") String dbn);
}
package com.example.nycschools.ui.main;

import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.nycschools.R;
import com.example.nycschools.databinding.SchoolsListFragmentBinding;

/**
 * SchoolsListFragment Fragment displayed on app launch which fetches the list of NYC Schools and shows it to the user
 */

public class SchoolsListFragment extends Fragment {

    private SchoolsListViewModel mViewModel;
    private SchoolsListFragmentBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Using data binding
        binding = SchoolsListFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // instantiate the view model
        mViewModel = new ViewModelProvider(this).get(SchoolsListViewModel.class);

        configureUI();
    }

    private void configureUI() {
        // observe the liveDate in the viewmodel for responses from the webservice call
        mViewModel.getSchools().observe(this, schoolsListResponses -> {
            // display error message on screen if data returned is empty
            if(schoolsListResponses == null) {
                binding.error.setVisibility(View.VISIBLE);
            } else {
                // set the adapter on the RecyclerView
                // Pass in the data and a callback function to handle click events
                binding.schoolsList.setAdapter(new SchoolsListAdapter(schoolsListResponses, position -> {
                    // navigate to the SchoolDetailsFragment when an item is clicked on the RecyclerView
                    // this navigation method could be improved with Android Navigation Component
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.main, new SchoolDetailsFragment(schoolsListResponses.get(position).getDbn()))
                            .addToBackStack(null)
                            .commit();
                }));
            }
        });

        // set the layout manager on the RecyclerView
        binding.schoolsList.setLayoutManager(new LinearLayoutManager(getContext()));

        // observing the liveData in viewmodel to check if data has been received or not
        // toggle the progress bar
        mViewModel.loading.observe(this, loading -> binding.loader.setVisibility(loading ? View.VISIBLE : View.GONE));
    }

    public interface OnItemClickListener {
        void onItemClicked(int position);
    }
}
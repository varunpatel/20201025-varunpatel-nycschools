package com.example.nycschools.ui.main;

import androidx.lifecycle.ViewModel;

import com.example.nycschools.retrofit.API;
import com.example.nycschools.retrofit.RetrofitClient;

// BaseViewModel extends from ViewModel and just initializes RetrofitClient
// BaseViewModel is a common class that all the viewmodels in the app will extend from
// Any shared code between viewmodels can be added here
public class BaseViewModel extends ViewModel {
    public static API api;

    public BaseViewModel() {
        super();
        api = RetrofitClient.getInstance().create(API.class);
    }
}

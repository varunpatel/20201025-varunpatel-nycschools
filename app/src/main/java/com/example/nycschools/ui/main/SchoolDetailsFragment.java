package com.example.nycschools.ui.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.nycschools.databinding.SchoolDetailsFragmentBinding;

public class SchoolDetailsFragment extends Fragment {

    private SchoolDetailsViewModel mViewModel;
    private SchoolDetailsFragmentBinding binding;
    private String dbn;

    public SchoolDetailsFragment(String key) {
        dbn = key;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = SchoolDetailsFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(SchoolDetailsViewModel.class);

        configureUI();
    }

    private void configureUI() {
        mViewModel.fetchData(dbn);

        mViewModel.schoolDetails.observe(this, schoolsDetailsResponses -> {
            binding.setSchoolDetails(schoolsDetailsResponses);
        });

        mViewModel.error.observe(this, error -> {
            binding.setSchoolDetails(null);
        });

        mViewModel.loading.observe(this, loading -> binding.loader.setVisibility(loading ? View.VISIBLE : View.GONE));
    }
}

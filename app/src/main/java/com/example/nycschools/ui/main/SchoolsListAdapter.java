package com.example.nycschools.ui.main;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nycschools.R;
import com.example.nycschools.model.SchoolsListResponse;

import java.util.List;

public class SchoolsListAdapter extends RecyclerView.Adapter<SchoolsListAdapter.SchoolsListViewHolder> {
    private final List<SchoolsListResponse> schoolsList;
    private SchoolsListFragment.OnItemClickListener listener;

    /**
     *
     * @param schoolNameList List of the responses received from the server
     * @param clickListener Callback function to handle click events
     */
    public SchoolsListAdapter(List<SchoolsListResponse> schoolNameList, SchoolsListFragment.OnItemClickListener clickListener) {
        schoolsList = schoolNameList;
        listener = clickListener;
    }

    @NonNull
    @Override
    public SchoolsListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SchoolsListViewHolder((TextView) LayoutInflater.from(parent.getContext()).inflate(R.layout.school_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SchoolsListViewHolder holder, int position) {
        holder.bind(schoolsList.get(position).getSchoolName(), position, listener);
    }


    @Override
    public int getItemCount() {
        return schoolsList.size();
    }

    static class SchoolsListViewHolder extends RecyclerView.ViewHolder {
        private final TextView schoolNameTextView;

        public SchoolsListViewHolder(@NonNull TextView textView) {
            super(textView);
            schoolNameTextView = textView;
        }

        /**
         * Function which binds the data to the UI
         * @param value String name of the school to display
         * @param position int position of the item in the list
         * @param clickListener OnItemClickListener callback function to handle click events
         */
        public void bind(String value, int position, SchoolsListFragment.OnItemClickListener clickListener) {
            schoolNameTextView.setText(value);
            schoolNameTextView.setOnClickListener(view -> clickListener.onItemClicked(position));
        }
    }
}

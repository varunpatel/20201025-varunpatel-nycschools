package com.example.nycschools.ui.main;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.nycschools.model.SchoolsDetailsResponse;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class SchoolDetailsViewModel extends BaseViewModel {

    // Composite Disposable to hold Disposable observables
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    // liveData to hold a custom error message to display on the UI
    public MutableLiveData<String> error = new MutableLiveData<>();
    // liveData to hold a Boolean if the data has loaded or not
    public MutableLiveData<Boolean> loading = new MutableLiveData<>();

    // private mutableLiveDate to hold the responses from the API
    private MutableLiveData<SchoolsDetailsResponse> _schoolDetails = new MutableLiveData<>();
    // public liveData which is exposed to the fragment (read-only) to build the UI
    public LiveData<SchoolsDetailsResponse> schoolDetails = _schoolDetails;

    /**
     * Function to make the API call and fetch data
     * @param dbn String argument to pass into the webservice call
     */
    public void fetchData(String dbn) {
        loading.postValue(true);
        compositeDisposable.add(api.getDetails(dbn)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<SchoolsDetailsResponse>>() {
                    @Override
                    public void onSuccess(@NonNull List<SchoolsDetailsResponse> schoolsDetailsResponses) {
                        loading.postValue(false);
                        if(!schoolsDetailsResponses.isEmpty()) _schoolDetails.postValue(schoolsDetailsResponses.get(0));
                        else {
                            SchoolsDetailsResponse response = new SchoolsDetailsResponse();
                            response.setSchoolName("NO DATA AVAILABLE");
                            _schoolDetails.postValue(response);
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        loading.postValue(false);
                        error.postValue(e.getMessage());
                    }
                }));
    }

    @Override
    protected void onCleared() {
        compositeDisposable.dispose();
        _schoolDetails = null;
        super.onCleared();
    }
}

package com.example.nycschools.ui.main;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.nycschools.model.SchoolsListResponse;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class SchoolsListViewModel extends BaseViewModel {

    // Composite Disposable to hold Disposable observables
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    // liveData to hold a Boolean if the data has loaded or not
    public MutableLiveData<Boolean> loading = new MutableLiveData<>();

    // private mutableLiveDate to hold the responses from the API
    private MutableLiveData<List<SchoolsListResponse>> schools;
    // public liveData which is exposed to the fragment (read-only) to build the UI
    public LiveData<List<SchoolsListResponse>> getSchools() {
        if(schools == null) {
            schools = new MutableLiveData<>();
            fetchData();
        }

        return schools;
    }

    /**
     * Function to make the API call and fetch data
     */
    private void fetchData() {
        loading.postValue(true);
        compositeDisposable.add(api.getSchools()
        .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<SchoolsListResponse>>() {
                    @Override
                    public void onSuccess(@NonNull List<SchoolsListResponse> schoolsListResponses) {
                        loading.postValue(false);
                        schools.postValue(schoolsListResponses);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        loading.postValue(false);
                        schools.postValue(null);
                    }
                }));
    }

    @Override
    protected void onCleared() {
        compositeDisposable.dispose();
        schools = null;
        super.onCleared();
    }
}
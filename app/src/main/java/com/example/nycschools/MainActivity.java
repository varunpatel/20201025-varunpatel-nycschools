package com.example.nycschools;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.nycschools.ui.main.BaseViewModel;
import com.example.nycschools.ui.main.SchoolsListFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        if (savedInstanceState == null) {
            // instantiate and add the fragment that will display the list of schools in NYC to the MainActivity
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, new SchoolsListFragment())
                    .commit();
        }

        // creating an instance of the BaseViewModel
        // this can be eliminated with the use of Dependency Injection
        new BaseViewModel();
    }
}


/**
 * Libraries included in the project:
 * - RxJava: Library for reactive and asynchronous tasks
 * - Retrofit: A type safe HTTP Client to make webservice calls
 * - GSON: A Java Library used to serialize/deserialize objects
 * - Data/View Binding: Library part of Android Jetpack which makes the communication between views and code
 *
 * Things I would have used for larger project:
 * - Navigation Components: Create a navigation graph and use Android's navigation component to control navigation between fragments
 * - Dagger: Dependency Injection Library for injecting dependencies into classes done at runtime
 * - Repository Pattern: Segregate data sources and rest of the app in different modules
 *
 * Other minor things I overlooked in the interest of expediency:
 * - Better UI: I kept the UI very simple, no complex fonts/colors/styles
 * - Better File Structure: I would separate the files in their individual modules. For example, fragments, viewmodels, adapters, viewholders, etc
 * - Strings.xml: The constant strings used in the app are hardcoded into the XML itself, these can go into the strings.xml file
 * - Styles.xml: Custom styles would go here which would apply to the UI elements in the app
 * - School List: Instead of displaying just the names of the schools in the list, I wish to also include things like phone number, address, etc
 *                  and create a custom list item
 *
 * Would also prefer to use Kotlin as it the recommended language by Google. With Kotlin features like coroutines, extension functions, etc
 *  can be utilized.
 *
 * Thanks!
 **/